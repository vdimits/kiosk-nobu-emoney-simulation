﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace EDC_Nobu_1._1
{
    class Program
    {
        public static SerialPort port = new SerialPort("COM9",9600, Parity.None, 8, StopBits.One);
        private static byte[] STX = { 0x02 };
        private static byte[] Version = { 0x01 };      
        private static byte[] TransType = { 0x51 };
        private static byte[] TransSubType = { 0x30 };
        private static byte[] Data = { 0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x32,0x30,0x30,0x30,0x30 };
        private static byte[] ETX = { 0x03 };
        private static byte[] CRC = { 0x61 };

        static void Main(string[] args)
        {
            try
            {
                port.Open();
                Send();

                byte[] buf = new byte[port.ReadBufferSize];
                string Final = "";
                int bytesToRead;

                while ((bytesToRead = port.Read(buf, 0, buf.Length)) > 0)
                {
                    //port.Read(buf, 0, bytesToRead);
                    Final = Encoding.ASCII.GetString(buf, 0, bytesToRead);
                    //Final = BitConverter.ToString(buf, 0, bytesToRead);
                    Console.WriteLine(Final);
                }

                if (port.IsOpen)
                    port.Close();

                Console.ReadKey();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }
        public static void Send()
        {
            byte[] messageSent = new byte[40];
            messageSent = STX.Concat(Version.Concat(TransType.Concat(TransSubType.Concat(Data.Concat(ETX.Concat(CRC)))))).ToArray();
            port.Write(messageSent, 0, messageSent.Length);
        }

        //public static string Receive()
        //{
        //    byte[] buf = new byte[port.ReadBufferSize];
        //    string Final = "";
        //    int bytesToRead;

        //    while ((bytesToRead = port.Read(buf, 0, buf.Length)) > 0)
        //    {
        //        port.Read(buf, 0, bytesToRead);
        //        Final = Encoding.ASCII.GetString(buf, 0, bytesToRead);
        //        Final = BitConverter.ToString(buf, 0, bytesToRead);
        //        return Final;
        //    }
        //    return "";
        //}

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static string HextoString(string InputText)
        {

            byte[] bb = Enumerable.Range(0, InputText.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(InputText.Substring(x, 2), 16))
                             .ToArray();
            return System.Text.Encoding.ASCII.GetString(bb);
        }
    }
}
